﻿using UnityEngine;
using System.Collections;

public class TowerFireBehavior : EnemysBehavior
{
    [SerializeField]
    float rotateSpeed, fireActiveTime;
    float currentFireActiveTime;

    [SerializeField]
    TowerFireAttack myFire;


    void Start()
    {
        transform.position += Vector3.up * 1.2f;
        StartStatus();
        shipTransform = GameObject.Find("AllShip").transform;
        myFire.damage = statusSO.fireDamage;
    }


    void Update()
    {
        if (GameManager.instance.gameTime == 0)
        {
            if (!stopped)
            {
                Stop();
                stopped = true;
            }
            return;
        }
        RotateMove();

        if (currentFireActiveTime < fireActiveTime)
        {
            OnFire();
        }
        else 
        {
            Recharg();
        }

    }


    void RotateMove()
    {
        transform.Rotate(0,rotateSpeed * Time.deltaTime * GameManager.Instance.gameTime,0);
    }


    void OnFire()
    {
        currentFireActiveTime += Time.deltaTime * GameManager.Instance.gameTime;
        currentRechargTime = 0;

        if (!myFire.gameObject.activeInHierarchy)
        {
            myFire.gameObject.SetActive(true);
        }
    }


    void Recharg() 
    {
        currentRechargTime += Time.deltaTime * GameManager.Instance.gameTime;

        if (currentRechargTime > statusSO.fireRechargTime) 
        {
            currentFireActiveTime = 0;    
        }

        if (myFire.gameObject.activeInHierarchy)
        {
            myFire.gameObject.SetActive(false);
        }
    }

}
