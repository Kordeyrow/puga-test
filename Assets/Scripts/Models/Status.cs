﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

public class Status : MonoBehaviour
{

    [Header("Settings")]
    public ShipType myType;

    
    [SerializeField] private List<StatusLevel> allStatus;
    [Range(1, 5)] public int healthLevel;
    [Range(1, 5)] public int attackLevel;
    [Range(1, 5)] public int speedLevel;

    
    [Header("Behaviour")]
    [SerializeField] protected IntReference damage; public int Damage { get { return damage.Value; } }
    [SerializeField] protected IntReference currentLife; public int CurrentLife { get { return currentLife.Value; } }
    [SerializeField] protected IntReference speed; public int Speed { get { return speed.Value; } }

    public int StartCurrentLife { get { return allStatus[healthLevel - 1].startHealth.Value; } }
    public int StartDamage { get { return allStatus[attackLevel - 1].startAttack.Value; } }
    public int StartSpeed { get { return (int) allStatus[speedLevel - 1].startSpeed.Value; } }

    [SerializeField] UnityEvent onTakeDamage;


    protected void StatusSetup()
    {
        currentLife.Value = StartCurrentLife;
        damage.Value = StartDamage;
        speed.Value = (int)StartSpeed;
    }

    public void TakeDamage(float applyDamage)
    {
        onTakeDamage.Invoke();
        currentLife.Value -= (int)applyDamage;
    }

    public void Death(float dropTax = 0, GameObject ob = null)
    {
        Destroy(this.gameObject);

        if (myType == ShipType.ENEMY)
        {
            int chance = Random.Range(0, 101);
            if (chance >= 100 - (int)(dropTax * 100))
            {
                Instantiate(ob, transform.position, Quaternion.identity);
            }
        }
    }
}