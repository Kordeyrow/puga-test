﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] UnityEvent onEndGame;
    [SerializeField] UnityEvent onWinGame;
    [SerializeField] UnityEvent onLoseGame;
    [SerializeField] UnityEvent onDisableGame;
    [SerializeField] UnityEvent onStartGame;
    [SerializeField] UnityEvent onPause;
    [SerializeField] UnityEvent onUnPause;

    [Header("Singleton")]
    public static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    [Header("References")]
    public Transform pivotToRestart;

    public GameObject ship;

    [Header("Behaviour")]
    [HideInInspector] public float gameTime = 1;
    [HideInInspector] public bool endGame;
    [SerializeField] FloatReference startTime;
    [SerializeField] FloatReference gameTimer;

    bool isPaused;

    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Time.timeScale = 1;
        onStartGame.Invoke();
        gameTimer.Value = startTime.Value;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            TogglePause();

        if (!endGame)
        {
            CountGameTimer();
            if (gameTimer.Value <= 0)
                EndGame(true);
        }
    }

    public void TogglePause()
    {
        isPaused = !isPaused;
        if (isPaused)
        {
            Time.timeScale = 0;
            onPause.Invoke();
        }
        else
        {
            Time.timeScale = 1;
            onUnPause.Invoke();
        }
    }

    void CountGameTimer()
    {
        gameTimer.Value = Mathf.Max(0, gameTimer.Value - Time.deltaTime);
    }

    public void EndGame(bool won)
    {
        endGame = true;
        SpawnManager.Instance.spawnAble = false;
        gameTime = 0;
        onEndGame.Invoke();

        if (won)
            WonGame();
        else
            LostGame();
    }

    void WonGame()
    {
        onWinGame.Invoke();
    }

    void LostGame()
    {
        onLoseGame.Invoke();
    }


    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //endGame = false;
        //ship.transform.position = new Vector3(pivotToRestart.position.x, ship.transform.position.y, pivotToRestart.position.z);
        //ship.GetComponent<ShipController>().SetHealth(100);
        //ship.GetComponent<ShipController>().EnebleMesh(true);
        //SpawnManager.Instance.DestroyerAllEnemy();
        //SpawnManager.Instance.spawnAble = true;
        //gameTime = 1;
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
        onDisableGame.Invoke();
        PlayerPrefs.Save();
    }
}
