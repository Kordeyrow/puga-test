﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Events;

public class ShipController : Status
{
    [Header("Reference")]
    public GameObject shield;
    public GameObject cannon;
    public GameObject cannonLocalSpawn;
    public List<SecondaryCannon> allSecondaryCannon;
    public List<DroneBehavior> allDrones;
    [SerializeField] IntReference selectedCannon;
    [SerializeField] IntReference selectedDrone;
    public MagaBombBehavior bombSpecial;
    public FireBehavior fireSpecial;
    public LazerBehavior lazerSpecial;
    public List<GameObject> Meshs;

    [Header("Settings")]
    public bool inputPC;
    public bool smoothRotateShield;
    public float shieldSpeedToSmoothRotation;
    public float angleToRotateShield;
    [Range(0, 1)] public float sensibilityInput;
    public float delayInputTimeToRotateShield;
    public float shootForSeconds;
    public BulletType bullet;
    public SecondaryCannonType mySecondaryCannonType;
    public DroneType myDroneType;
    public List<ManaStatus> mana;
    [Range(1, 3)] public int manaLevel;
    public SpecialType currentSpecialAble;
    public float slowMotionMaxTime;
    public float slowMotionTimeToFullRecharg;
    [Range(0, 1)] public float slowMotionEffect;

    [Header("Behaviour")]
    bool inStun;
    float currentStunTime;
    float stunTime;
    float currentTimeToRotateShield;
    [HideInInspector] public Vector3 targetToShoot;
    float timeToShoot;
    float currentTimeToShoot;
    SecondaryCannon currentCannon;
    private int currentMana;
    [HideInInspector] public int CurrentManaToSpecial { get { return currentMana; } set { currentMana = value; onChangeMana.Invoke(currentMana);
            canUseSpecial.Invoke(CurrentManaToSpecial >= bombSpecial.status[bombSpecial.megaBombLevel - 1].manaCost) ;
        } }
    bool slowMotionEnabled;
    float specialRechargTime;
    float currentSpecialRechargTime;
    float currentslowMotionTime;
    
    enum eCurrentMoving { None, Up, Down, Left, Right, UpRight, UpLeft, DownRight, DownLeft}
    eCurrentMoving currentMoving;
    float movingSmoothFactor;
    bool justSetMoving;

    bool isShootInput = false;
    public TextMeshProUGUI debugTxt;
    Vector3 shootPosition;

    [SerializeField] UnityEvent onShoot;
    [SerializeField] UnityEventInt onChangeMana;
    [SerializeField] UnityEventBool canUseSpecial;

    void Start()
    {
        StatusSetup();
        SetShootRate();
        SetCustom();

        currentslowMotionTime = slowMotionMaxTime;
    }

    void Update()
    {
        if (GameManager.Instance.endGame) return;
        TestIsDead();

        //Updates
        UpdateTimers();
        //UpdateTargetToShoot();
        UpdateShieldRotation();

        if (inStun) { CountStunTime(); return; }

        //Inputs
        TryMove();
        TryShoot();
        TrySlowmotion();
        TrySpecial();

        //Tests
        TestSlowMotion();
        justSetMoving = false;
    }

    public void SetCustom()
    {
        mySecondaryCannonType = (SecondaryCannonType)selectedCannon.Value;
        myDroneType = (DroneType)selectedDrone.Value;
        SetCurrentSecondaryCannon(mySecondaryCannonType);
        SetCurrentDrone(myDroneType);
        SetSpecialStats();
    }

    void TestIsDead()
    {
        if (currentLife.Value <= 0)
        {
            EnebleMesh(false);
            GameManager.Instance.EndGame(false);
        }
    }

    private void TestSlowMotion()
    {
        if (slowMotionEnabled)
        {
            SlowMotionEffect();
        }
        else if (currentslowMotionTime < slowMotionMaxTime)
        {
            RechargSlowMotion();
        }
    }

    private void UpdateTimers()
    {
        currentTimeToShoot += Time.deltaTime * GameManager.Instance.gameTime;
        currentSpecialRechargTime += Time.deltaTime * GameManager.Instance.gameTime;
    }

    private void TrySlowmotion()
    {
        if (Input.GetButtonDown("Slow Motion"))
        {
            SlowMotionEneble(true);
        }
    }

    void TrySpecial()
    {
        if (Input.GetButtonDown("Special"))
        {
            if (currentSpecialRechargTime > specialRechargTime)
            {
                ActiveSpecial();
            }
        }
    }

    public void BtnSpecialPressed()
    {
        if (currentSpecialRechargTime > specialRechargTime)
        {
            ActiveSpecial();
        }
    }

    void TryMove()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        
        float horizontal = 0, vertical = 0;

        switch (currentMoving)
        {
            case eCurrentMoving.None:
                break;
            case eCurrentMoving.Up:
                vertical = 1;
                break;
            case eCurrentMoving.Down:
                vertical = -1;
                break;
            case eCurrentMoving.Left:
                horizontal = -1;
                break;
            case eCurrentMoving.Right:
                horizontal = 1;
                break;
            case eCurrentMoving.UpRight:
                horizontal = 1;
                vertical = 1;
                break;
            case eCurrentMoving.UpLeft:
                horizontal = -1;
                vertical = 1;
                break;
            case eCurrentMoving.DownRight:
                horizontal = 1;
                vertical = -1;
                break;
            case eCurrentMoving.DownLeft:
                horizontal = -1;
                vertical = -1;
                break;
        }
        if (currentMoving == eCurrentMoving.None)
            movingSmoothFactor = Mathf.Max(0, movingSmoothFactor - Time.deltaTime * 0.4f);
        else
            movingSmoothFactor = Mathf.Max(0.5f, Mathf.Min(1.2f, movingSmoothFactor + Time.deltaTime));
        gameObject.transform.Translate(horizontal * movingSmoothFactor * speed.Value * Time.deltaTime * GameManager.Instance.gameTime,
                                        0,
                                        vertical * movingSmoothFactor * speed.Value * Time.deltaTime * GameManager.Instance.gameTime);
#else

        gameObject.transform.Translate(Input.GetAxis("Horizontal") * speed.Value * Time.deltaTime * GameManager.Instance.gameTime,
                                        0,
                                        Input.GetAxis("Vertical") * speed.Value * Time.deltaTime * GameManager.Instance.gameTime);
#endif
    }

    public void SetCurrentMoving(int _index)
    {
        justSetMoving = true;
        currentMoving = (eCurrentMoving)_index;
    }

    void TryShoot()
    {
        if (justSetMoving)
        {
            justSetMoving = false;
            return;
        }

        if (isShootInput)
        {
            Shoot();
        }
    }

    //void UpdateTargetToShoot()
    //{
    //    if (inputPC)
    //    {
    //    }
    //    else
    //        UpdateTargetToShootJoystick();
    //}

    void UpdateShieldRotation()
    {
        if (smoothRotateShield)
        {
            UpdateSmoothRotateShield();
        }
        else
        {
            UpdateRotateShield();
        }
    }

    void CountStunTime()
    {
        currentStunTime += Time.deltaTime * GameManager.Instance.gameTime;
        if (currentStunTime > stunTime)
        {
            inStun = false;
            stunTime = 0;
        }
    }

    void UpdateRotateShield()
    {
        if (Input.GetAxis("RotateShield") < 0.19f && Input.GetAxis("RotateShield") > 0.19f)
        {
            currentTimeToRotateShield = delayInputTimeToRotateShield;
        }

        currentTimeToRotateShield += Time.deltaTime * GameManager.Instance.gameTime;


        shield.transform.Rotate(new Vector3(0, 120 * Time.deltaTime, 0));
        //if (currentTimeToRotateShield >= delayInputTimeToRotateShield)
        //{
        //    if (Input.GetAxis("RotateShield") > sensibilityInput)
        //    {
        //        shield.transform.Rotate(new Vector3(0, -angleToRotateShield, 0));
        //        currentTimeToRotateShield = 0;
        //    }
        //    else if (Input.GetAxis("RotateShield") < -sensibilityInput)
        //    {
        //        shield.transform.Rotate(new Vector3(0, angleToRotateShield, 0));
        //        currentTimeToRotateShield = 0;
        //    }
        //}
    }


    void UpdateSmoothRotateShield()
    {
        shield.transform.Rotate(new Vector3(0, shieldSpeedToSmoothRotation * Input.GetAxis("RotateShield") * Time.deltaTime * GameManager.Instance.gameTime, 0));
    }


    void UpdateTargetToShoot(Vector3 pos)
    {
        float localX = 0;
        float localZ = 0;

        Ray ray = Camera.main.ScreenPointToRay(pos);
        RaycastHit[] hit;

        hit = Physics.RaycastAll(ray);

        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i].transform.CompareTag("Plataform") || hit[i].transform.CompareTag("Wall"))
            {
                localX = hit[i].point.x;
                localZ = hit[i].point.z;
                break;
            }
        }

        targetToShoot = new Vector3(localX, cannon.transform.position.y, localZ);

        cannon.transform.LookAt(targetToShoot);

    }


    void UpdateTargetToShootJoystick()
    {
        float localX = Input.GetAxis("Horizontal Rotation");
        float localZ = Input.GetAxis("Vertical Rotation");

        targetToShoot = new Vector3(gameObject.transform.position.x + localX, cannon.transform.position.y, gameObject.transform.position.z + localZ);
        if (targetToShoot != gameObject.transform.position)
        {
            cannon.transform.LookAt(targetToShoot);
            if (Vector2.SqrMagnitude(new Vector2(localX, localZ)) > 0.7f)
                Shoot();
        }
    }

    public void StopInputShooting()
    {
        isShootInput = false;
    }

    public void SetShootPosition(BaseEventData _data)
    {
        if (Time.timeScale == 0)
            return;

        isShootInput = true;

        PointerEventData _p = (PointerEventData)_data;
        //Debug.Log(_p.position);
        //debugTxt.text = Camera.main.ScreenToWorldPoint(_p.position).ToString();

        RaycastHit hit = new RaycastHit();
        UpdateTargetToShoot(_p.position);


        //Ray ray = Camera.main.ScreenPointToRay(_p.position);
        //Physics.Raycast(ray, out hit);
        //if (hit.collider != null)
        //{
        //    shootPosition.x = hit.point.x;
        //    shootPosition.z = hit.point.z;
        //}

        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Debug.DrawRay(ray.origin, ray.direction * 10, Color.green);

        //RaycastHit hit = new RaycastHit();
        //if (Physics.Raycast(ray, hit))
        //{

        //    if (hit.collider.gameObject.tag == "blue" && Input.GetMouseButtonUp(1))
        //    {


        //        Debug.Log("Hit blue");

        //    }
        //
    }
        void Shoot()
    {
        if (currentTimeToShoot > timeToShoot)
        {
            GameObject currentBullet = InstanceManager.Instance.InstanceBullet(bullet, cannonLocalSpawn.transform.position, cannonLocalSpawn.transform.rotation);
            currentBullet.GetComponent<BulletBehavior>().SetBulletStats(damage.Value, this.myType);

            onShoot.Invoke();

            if (currentCannon != null)
            {
                for (int i = 0; i < currentCannon.cannonPoint.Count; i++)
                {
                    GameObject currentSecondaryBullet = InstanceManager.Instance.InstanceBullet(bullet, currentCannon.cannonPoint[i].transform.position, currentCannon.cannonPoint[i].transform.rotation);
                    currentSecondaryBullet.GetComponent<BulletBehavior>().SetBulletStats((int)(damage.Value * currentCannon.status[currentCannon.level - 1].DamageIndex), this.myType);
                }
            }

            currentTimeToShoot = 0;
        }
    }

    public void SetHealth(int _newHealth)
    {
        currentLife.Value = _newHealth;
    }

    void SetShootRate()
    {
        timeToShoot = 1 / shootForSeconds;
    }


    void SlowMotionEffect()
    {
        currentslowMotionTime -= Time.deltaTime;

        if (currentslowMotionTime <= 0)
        {
            SlowMotionEneble(false);
        }
    }


    void RechargSlowMotion()
    {
        currentslowMotionTime += Time.deltaTime / (slowMotionTimeToFullRecharg / slowMotionMaxTime);

        if (currentslowMotionTime > slowMotionMaxTime)
            currentslowMotionTime = slowMotionMaxTime;
    }


    void SlowMotionEneble(bool enable)
    {
        if (enable)
        {
            GameManager.Instance.gameTime = slowMotionEffect;
            slowMotionEnabled = true;
        }
        else
        {
            GameManager.Instance.gameTime = 1;
            slowMotionEnabled = false;
        }
    }


    public void EnebleMesh(bool state)
    {
        foreach (GameObject ob in Meshs)
        {
            ob.SetActive(state);
        }
    }


    public void SetCurrentSecondaryCannon(SecondaryCannonType currentType)
    {
        for (int i = 0; i < allSecondaryCannon.Count; i++)
        {
            if (allSecondaryCannon[i].myCannonType == currentType)
            {
                currentCannon = allSecondaryCannon[i];
                allSecondaryCannon[i].gameObject.SetActive(true);
                Meshs.Add(allSecondaryCannon[i].gameObject);
            }
            else
            {
                allSecondaryCannon[i].gameObject.SetActive(false);
                if (Meshs.Contains(allSecondaryCannon[i].gameObject))
                    Meshs.Remove(allSecondaryCannon[i].gameObject);
            }
        }
    }


    public void SetCurrentDrone(DroneType currentType)
    {
        for (int i = 0; i < allDrones.Count; i++)
        {
            if (allDrones[i].thisDroneType == myDroneType)
            {
                allDrones[i].gameObject.SetActive(true);
                Meshs.Add(allDrones[i].gameObject);
            }
            else
            {
                allDrones[i].gameObject.SetActive(false);
                if (Meshs.Contains(allDrones[i].gameObject))
                    Meshs.Remove(allDrones[i].gameObject);
            }

        }
    }


    void SetSpecialStats()
    {
        switch (currentSpecialAble)
        {
            case SpecialType.BOMB:
                specialRechargTime = bombSpecial.status[bombSpecial.megaBombLevel - 1].durationRecharg;
                break;
            case SpecialType.LAZER:
                specialRechargTime = lazerSpecial.status[lazerSpecial.lazerLevel - 1].durationRecharg;
                break;
            case SpecialType.FIRE:
                specialRechargTime = fireSpecial.status[fireSpecial.fireLevel - 1].durationRecharg;
                break;
        }

        lazerSpecial.gameObject.SetActive(false);
        fireSpecial.gameObject.SetActive(false);

        currentSpecialRechargTime = specialRechargTime;
        CurrentManaToSpecial = mana[manaLevel-1].ManaTotal;
    }


     void ActiveSpecial()
    {
        switch (currentSpecialAble)
        {
            case SpecialType.BOMB:
                if (CurrentManaToSpecial >= bombSpecial.status[bombSpecial.megaBombLevel - 1].manaCost)
                {
                    Instantiate(bombSpecial, this.transform.position, Quaternion.identity);
                    currentSpecialRechargTime = 0;
                    CurrentManaToSpecial -= bombSpecial.status[bombSpecial.megaBombLevel - 1].manaCost;
                }
                break;
            case SpecialType.LAZER:
                if (CurrentManaToSpecial >= lazerSpecial.status[lazerSpecial.lazerLevel - 1].manaCost)
                {
                    lazerSpecial.gameObject.SetActive(true);
                    currentSpecialRechargTime = 0;
                    lazerSpecial.currentDuration = 0;
                    CurrentManaToSpecial -= lazerSpecial.status[lazerSpecial.lazerLevel - 1].manaCost;
                }
                break;
            case SpecialType.FIRE:
                if (CurrentManaToSpecial >= fireSpecial.status[fireSpecial.fireLevel - 1].manaCost)
                {
                    fireSpecial.gameObject.SetActive(true);
                    fireSpecial.currentDuration = 0;
                    currentSpecialRechargTime = 0;
                    CurrentManaToSpecial -= fireSpecial.status[fireSpecial.fireLevel - 1].manaCost;
                }
                break;
        }
    }


    public void EnableStun(float newStunTime)
    {
        if (newStunTime > stunTime)
        {
            stunTime = newStunTime;
        }

        currentStunTime = 0;
        inStun = true;

    }
}

[System.Serializable]
public class ManaStatus
{
    public int ManaTotal;
}