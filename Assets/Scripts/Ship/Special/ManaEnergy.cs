﻿using UnityEngine;

public class ManaEnergy : MonoBehaviour
{
    [Header("Settings")]
    public int manaRechargValue;
    public float timeToDestroy;


    void Start()
    {
        Destroy(this.gameObject, timeToDestroy);
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Ship"))
        {
            if (RechargMana(other.GetComponent<ShipController>()))
                Destroy(this.gameObject);
        }
    }


    bool RechargMana(ShipController otherShip)
    {
        if (otherShip.CurrentManaToSpecial >= otherShip.mana[otherShip.manaLevel-1].ManaTotal)
            return false;

        otherShip.CurrentManaToSpecial += manaRechargValue;
        Mathf.Clamp(otherShip.CurrentManaToSpecial, 0, otherShip.mana[otherShip.manaLevel - 1].ManaTotal);

        return true;
    }
}
