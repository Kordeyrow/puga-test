﻿using UnityEngine;
using UnityEngine.Events;

public class CurrencyManager : MonoBehaviour
{
    public static CurrencyManager instance;
    public static CurrencyManager Instance { get { return instance; } }


    [Header("References")]
    public GameObject coinMesh;

    [Header("Settings")]
    public int maxCountForCurrencys;

    [Header("Behaviour")]
    [SerializeField] public IntReference totalCurrencys;

    [SerializeField] UnityEvent onGetMoney;


    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        totalCurrencys.Value = 0;
    }


    public void AddCurrency(int valueToAdd)
    {
        totalCurrencys.Value += valueToAdd;
        onGetMoney.Invoke();
    }
}
