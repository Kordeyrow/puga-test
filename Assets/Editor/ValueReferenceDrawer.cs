﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(ValueReference), true)]
public class FloatReferenceDrawer : PropertyDrawer
{
    /// <summary>
    /// Options to display in the popup to select constant or variable.
    /// </summary>
    private readonly string[] popupOptions =
        { "Use Constant", "Use SO", "Use Component" };

    /// <summary> Cached style to use to draw the popup button. </summary>
    private GUIStyle popupStyle;

    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        if (popupStyle == null)
        {
            popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
            popupStyle.imagePosition = ImagePosition.ImageOnly;
        }

        label = EditorGUI.BeginProperty(rect, label, property);
        rect = EditorGUI.PrefixLabel(rect, label);

        EditorGUI.BeginChangeCheck();

        // Get properties
        SerializedProperty valueType = property.FindPropertyRelative("valueType");
        SerializedProperty constantValue = property.FindPropertyRelative("constantValue");
        SerializedProperty SOValue = property.FindPropertyRelative("SOValue");
        SerializedProperty componentValue = property.FindPropertyRelative("componentValue");

        SerializedProperty[] fields = new SerializedProperty[] { constantValue, SOValue, componentValue };

        // Calculate rect for configuration button
        Rect buttonRect = new Rect(rect);
        buttonRect.yMin += popupStyle.margin.top;
        buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
        rect.xMin = buttonRect.xMax;

        // Store old indent level and set it to 0, the PrefixLabel takes care of it
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        int result = EditorGUI.Popup(buttonRect, valueType.intValue , popupOptions, popupStyle);
        valueType.intValue = result;

        EditorGUI.PropertyField(rect,
            fields[valueType.intValue],
            GUIContent.none);

        if (EditorGUI.EndChangeCheck())
            property.serializedObject.ApplyModifiedProperties();

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }

    void GetObjectClasse(SerializedProperty property)
    {
        //var obj = fieldInfo.GetValue(property.serializedObject.targetObject);
        //ValueReference valueReference = obj as ValueReference;
        //if (obj.GetType().IsArray)
        //{
        //    var index = System.Convert.ToInt32(new string(property.propertyPath.Where(c => char.IsDigit(c)).ToArray()));
        //    valueReference = ((ValueReference[])obj)[index];
        //}
    }
}
#endif
