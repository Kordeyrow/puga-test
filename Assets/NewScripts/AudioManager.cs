﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum EAudioClips { Swipe_to_Left, Swipe_to_Right, Take_Damage, Hit, Button, Button2 }

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [SerializeField] AudioSource[] audioSources;
    [SerializeField] AudioClip[] audioClips;
    [SerializeField] float[] audioClipsVolume;
    public float CurrentSfxVolume { get; set; }
    public bool Muted { get; set; }
    int firstAudioSourceUsed;

    private void Awake()
    {
        Instance = this;
        CurrentSfxVolume = 1;
        audioSources = new AudioSource[10];
        for (int i = 0; i < audioSources.Length; i++)
            audioSources[i] = gameObject.AddComponent<AudioSource>();
    }

    //[EnumAction(typeof(EAudioClips))]
    //public void PlaySfx(int _clip)
    //{
    //    PlaySfx((EAudioClips)_clip);
    //}

    public void PlaySfxIndex(int _clipIndex)
    {
        //if (Muted || audioSources == null || audioSources.Length == 0 || _clipIndex >= audioClips.Length || _clipIndex >= audioClipsVolume.Length)
        //    return;

        //int audioSourceToPlay = GetFreeAudioSource();

        //if (audioSourceToPlay == -1)
        //    audioSourceToPlay = GetBetterAudioSource();

        //PlayAudio(audioSourceToPlay, _clipIndex);











        //AudioSource.PlayClipAtPoint(audioClips[_clipIndex], Camera.main.transform.position, audioClipsVolume[_clipIndex]);

        //AudioClip clip = FindAudioClipByName(_clip);
        //if (clip != null)
        //{
        //    AudioSource auds = gameObject.AddComponent<AudioSource>();
        //    auds.clip = FindAudioClipByName(_clip);
        //    auds.volume = CurrentSfxVolume;
        //    auds.Play();
        //    Destroy(auds, auds.clip.length);

        //    ////AudioSource.PlayClipAtPoint(clip, new Vector3(0, 0, 0));
        //}
    }

    int GetBetterAudioSourceIndex()
    {
        if (audioSources == null)
            return -1;
        float currentTime = 0;
        int index = 0;
        for (int i = 0; i < audioSources.Length; i++)
        {
            if (audioSources[i] == null)
                continue;    

            if (!audioSources[i].isPlaying)
                return i;
            if (audioSources[i].time / audioSources[i].clip.length > currentTime)
            {
                currentTime = audioSources[i].time;
                index = i;
            }
        }
        return index;
    }

    void PlayAudio(int _audioSourceIndex, int _clipIndex)
    {
        if (audioSources == null || audioSources.Length == 0 || _clipIndex >= audioClips.Length || _clipIndex >= audioClipsVolume.Length)
            return;

        audioSources[_audioSourceIndex].clip = audioClips[_clipIndex];
        audioSources[_audioSourceIndex].volume = audioClipsVolume[_clipIndex];
        audioSources[_audioSourceIndex].Play();
    }

    public void PlaySfx(int _clipIndex)
    {
        if (_clipIndex >= audioClips.Length || audioSources == null)
            return;

        AudioClip clip = audioClips[_clipIndex];
        if (clip != null)
        {
            AudioSource auds = audioSources[GetBetterAudioSourceIndex()];
            if (auds == null)
                return;
            auds.clip = clip;
            float volume = 1;
            if (_clipIndex < audioClipsVolume.Length)
                volume = audioClipsVolume[_clipIndex];
            auds.volume = volume;
            auds.Play();

            ////AudioSource.PlayClipAtPoint(clip, new Vector3(0, 0, 0));
        }
    }

    public AudioClip FindAudioClipByName(EAudioClips _clip)
    {
        return (new List<AudioClip>(audioClips)).Find(x => x.name == _clip.ToString());
    }
}
