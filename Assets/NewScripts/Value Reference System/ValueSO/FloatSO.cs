﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewFloatConstant", menuName = "Values/Constants/Float")]
public class FloatSO : ScriptableObject, IConstant
{
    [SerializeField] protected float _value;
    [TextArea] [SerializeField] protected string _tooltip; public string Tooltip { get { return _tooltip; } }
    public float Value { get { return _value; } }
    public float GDEditorValue { set { _value = value; } }
}
