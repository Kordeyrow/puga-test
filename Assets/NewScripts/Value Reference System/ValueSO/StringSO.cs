﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewStringConstant", menuName = "Values/Constants/String")]
public class StringSO : ScriptableObject, IConstant
{
    [SerializeField] protected string _value;
    [TextArea] [SerializeField] protected string _tooltip; public string Tooltip { get { return _tooltip; } }
    public string Value { get { return _value; } }
    public string GDEditorValue { set { _value = value; } }
}
