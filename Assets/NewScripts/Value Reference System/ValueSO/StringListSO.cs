﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewStringListConstant", menuName = "Values/Constants/StringList")]
public class StringListSO : ScriptableObject, IConstant
{
    [SerializeField] protected string[] _value;
    [TextArea] [SerializeField] protected string _tooltip; public string Tooltip { get { return _tooltip; } }
    public string[] Value { get { return _value; } }
    public string[] GDEditorValue { set { _value = value; } }
}

