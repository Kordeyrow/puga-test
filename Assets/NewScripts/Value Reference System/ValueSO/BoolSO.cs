﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewBoolConstant", menuName = "Values/Constants/Bool")]
public class BoolSO : ScriptableObject, IConstant
{
    [SerializeField] protected bool _value;
    [TextArea] [SerializeField] protected string _tooltip; public string Tooltip { get { return _tooltip; } }
    public bool Value { get { return _value; } }
    public bool GDEditorValue { set { _value = value; } }
}

public interface IConstant { }