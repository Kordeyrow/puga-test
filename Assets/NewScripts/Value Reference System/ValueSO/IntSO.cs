﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewIntConstant", menuName = "Values/Constants/Int")]
public class IntSO : ScriptableObject, IConstant
{
    [SerializeField] protected int _value;
    [TextArea] [SerializeField] protected string _tooltip; public string Tooltip { get { return _tooltip; } }
    public int Value { get { return _value; } }
    public int GDEditorValue { set { _value = value; } }
}
