﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewBoolVariable", menuName = "Values/Variables/Bool")]
public class BoolVariableSO : BoolSO, IVariable
{
    //[SerializeField] protected bool onPlayValue;
    [SerializeField] protected BoolReference defaultValue;
    [SerializeField] protected HideFlags persistenceStyle;
    [SerializeField] protected UnityEventBool onChangeEvent;
    private List<BoolVariableListener> listeners = new List<BoolVariableListener>();

    private void OnEnable()
    {
        // NAO invokar onChangeEvent no OnEnable! Parece que ele perde os metodos atrelados via inspector
        
        //if (autoSaveOnDisable.Value && PlayerPrefs.HasKey(this.name))
        //    _value = PlayerPrefs.GetInt(this.name);
        //else
            _value = defaultValue.Value;
        hideFlags = persistenceStyle;
    }
    public void SetValue(bool _newValue)
    {
        if (_value == _newValue)
            return;
        _value = _newValue;
        RaiseOnChangeEvent();
    }
    public void SetValue(BoolSO _floatSO)
    {
        if (_floatSO != null)
            SetValue(_floatSO.Value);
    }
    //public void SetValue(BoolComponent _floatComponent)
    //{
    //    if (_floatComponent != null)
    //        SetValue(_floatComponent.Value);
    //}

    protected void RaiseOnChangeEvent()
    {
        onChangeEvent.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnChangeEventRaised(this);
    }

    public void Toogle()
    {
        SetValue(!Value);
    }

    public void RegisterListener(BoolVariableListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(BoolVariableListener listener)
    {
        listeners.Remove(listener);
    }

    public void ResetToDefault()
    {
        SetValue(defaultValue.Value);
    }
}
