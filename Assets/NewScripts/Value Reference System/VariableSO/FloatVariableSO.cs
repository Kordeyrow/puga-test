﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewFloatVariable", menuName = "Values/Variables/Float")]
public class FloatVariableSO : FloatSO, IVariable
{
    //[SerializeField] protected float onPlayValue;
    [SerializeField] protected FloatReference defaultValue;
    [SerializeField] protected BoolReference onlyPositive;
    [SerializeField] protected HideFlags persistenceStyle;
    [SerializeField] protected UnityEventFloat onChangeEvent;
    private List<FloatVariableListener> listeners = new List<FloatVariableListener>();

    protected virtual void OnEnable()
    {
        // NAO invokar onChangeEvent no OnEnable! Parece que ele perde os metodos atrelados via inspector

        //if (autoSaveOnDisable.Value && PlayerPrefs.HasKey(this.name))
        //    _value = PlayerPrefs.GetInt(this.name);
        //else
            _value = defaultValue.Value;
        hideFlags = persistenceStyle;
    }
    public void SetValue(float _newValue)
    {
        if (onlyPositive.Value)
            _newValue = Mathf.Max(0, _newValue);
        if (_value == _newValue)
            return;
        _value = _newValue;
        RaiseOnChangeEvent();
    }
    public void SetValue(FloatSO _floatSO)
    {
        if (_floatSO == null)
            return;
        SetValue(_floatSO.Value);
    }
    //public void SetValue(FloatComponent _floatComponent)
    //{
    //    if (_floatComponent != null)
    //        SetValue(_floatComponent.Value);
    //}

    protected void RaiseOnChangeEvent()
    {
        onChangeEvent.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnChangeEventRaised(this);
    }

    public void Add(float _value)
    {
        SetValue(Value + _value);
    }
    public void Add(FloatSO _floatSO)
    {
        if (_floatSO == null)
            return;
        Add(_floatSO.Value);
    }
    public void Add(int _value)
    {
        Add((float)_value);
    }
    public void Add(IntSO _intSO)
    {
        if (_intSO == null)
            return;
        Add(_intSO.Value);
    }

    public void Subtract(float _value)
    {
        SetValue(Value - _value);
    }
    public void Subtract(FloatSO _floatSO)
    {
        if (_floatSO == null)
            return;
        Subtract(_floatSO.Value);
    }
    public void Subtract(int _value)
    {
        Subtract((float)_value);
    }
    public void Subtract(IntSO _intSO)
    {
        if (_intSO == null)
            return;
        Subtract(_intSO.Value);
    }

    public void RegisterListener(FloatVariableListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(FloatVariableListener listener)
    {
        listeners.Remove(listener);
    }

    public void ResetToDefault()
    {
        SetValue(defaultValue.Value);
    }
}
