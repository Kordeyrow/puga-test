﻿using System.Collections.Generic;
using UnityEngine;

public interface IVariable { }

[CreateAssetMenu(fileName = "NewTransformVariable", menuName = "Values/Variables/Transform")]
public class TransformVariableSO : TransformSO, IVariable
{
    //[SerializeField] protected Transform onPlayValue;
    [SerializeField] protected UnityEventTransform onChangeEvent;
    private List<TransformVariableListener> listeners = new List<TransformVariableListener>();

    //private void OnEnable()
    //{
    //    // NAO invokar onChangeEvent no OnEnable! Parece que ele perde os metodos atrelados via inspector
    //    _value = onPlayValue;
    //}
    public void SetValue(Transform _newValue)
    {
        if (_value == _newValue)
            return;
        _value = _newValue;
        RaiseOnChangeEvent();
    }
    public void SetValue(TransformSO transformSO)
    {
        if (transformSO != null)
            SetValue(transformSO.Value);
    }
    public void SetValue(TransformComponent _transformComponent)
    {
        onChangeEvent.Invoke(_value);
        if (_transformComponent != null)
            SetValue(_transformComponent.Value);
    }

    protected void RaiseOnChangeEvent()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnChangeEventRaised(this);
    }

    public void RegisterListener(TransformVariableListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(TransformVariableListener listener)
    {
        listeners.Remove(listener);
    }
}
