﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewStringVariable", menuName = "Values/Variables/String")]
public class StringVariableSO : StringSO, IVariable
{
    //[SerializeField] protected string onPlayValue;
    [SerializeField] protected StringReference defaultValue;
    [SerializeField] protected HideFlags persistenceStyle;
    [SerializeField] protected UnityEventString onChangeEvent;
    private List<StringVariableListener> listeners = new List<StringVariableListener>();

    private void OnEnable()
    {
        // NAO invokar onChangeEvent no OnEnable! Parece que ele perde os metodos atrelados via inspector

        //if (autoSaveOnDisable.Value && PlayerPrefs.HasKey(this.name))
        //    _value = PlayerPrefs.GetInt(this.name);
        //else
            _value = defaultValue.Value;
        hideFlags = persistenceStyle;
    }
    public void SetValue(string _newValue)
    {
        if (_value == _newValue)
            return;
        _value = _newValue;
        RaiseOnChangeEvent();
    }
    public void SetValue(StringSO _stringSO)
    {
        if (_stringSO == null)
            return;
        SetValue(_stringSO.Value);
    }
    //public void SetValue(StringComponent _stringComponent)
    //{
    //    if (_stringComponent == null)
    //        return;
    //    SetValue(_stringComponent.Value);
    //}

    protected void RaiseOnChangeEvent()
    {
        onChangeEvent.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnChangeEventRaised(this);
    }

    public void RegisterListener(StringVariableListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(StringVariableListener listener)
    {
        listeners.Remove(listener);
    }

    public void ResetToDefault()
    {
        SetValue(defaultValue.Value);
    }
}
