﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewIntVariable", menuName = "Values/Variables/Int")]
public class IntVariableSO : IntSO, IVariable
{
    //[SerializeField] protected int onPlayValue;
    [SerializeField] protected IntReference defaultValue;
    [SerializeField] protected BoolReference onlyPositive;
    [SerializeField] protected HideFlags persistenceStyle;
    [SerializeField] protected UnityEventInt onChangeEvent;
    [SerializeField] protected UnityEventInt onSetupEvent;
    [SerializeField] protected UnityEventInt onAddEvent;
    private List<IntVariableListener> listeners = new List<IntVariableListener>();
    bool wasSetUp = false;

    private void OnEnable()
    {
        // NAO invokar onChangeEvent no OnEnable! Parece que ele perde os metodos atrelados via inspector

        //if (autoSaveOnDisable.Value && PlayerPrefs.HasKey(this.name))
        //    _value = PlayerPrefs.GetInt(this.name);
        //else
        wasSetUp = false;
        _value = defaultValue.Value;
        hideFlags = persistenceStyle;
    }
    public void SetValue(int _newValue)
    {
        if (onlyPositive.Value)
            _newValue = Mathf.Max(0, _newValue);

        int oldValue = _value;

        _value = _newValue;
        if (!wasSetUp)
        {
            wasSetUp = true;
            RaiseOnSetUpEvent();
        }

        if (oldValue == _newValue)
            return;
        RaiseOnChangeEvent();
    }
    public void SetValue(IntSO _intSO)
    {
        if (_intSO == null)
            return;
        SetValue(_intSO.Value);
    }
    //public void SetValue(IntComponent _intComponent)
    //{
    //    if (_intComponent != null)
    //        SetValue(_intComponent.Value);
    //}

    protected void RaiseOnChangeEvent()
    {
        onChangeEvent.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnChangeEventRaised(this);
    }

    protected void RaiseOnSetUpEvent()
    {
        onSetupEvent.Invoke(_value);
        for (int i = listeners.Count - 1; i >= 0; i--)
            listeners[i].OnSetUpEventRaised(this);
    }

    public void Add(int _valueToAdd)
    {
        SetValue(Value + _valueToAdd);
    }
    public void Add(IntSO _intSO)
    {
        if (_intSO == null)
            return;
        Add(_intSO.Value);
    }
    public void Subtract(int _value)
    {
        SetValue(Value - _value);
    }
    public void Subtract(IntSO _intSO)
    {
        if (_intSO == null)
            return;
        Subtract(_intSO.Value);
    }

    public void RegisterListener(IntVariableListener listener)
    {
        listeners.Add(listener);
    }
    public void UnregisterListener(IntVariableListener listener)
    {
        listeners.Remove(listener);
    }

    public void ResetToDefault()
    {
        SetValue(defaultValue.Value);
    }
}
