﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class IntVariableListener : MonoBehaviour
{
    [SerializeField] bool invokeOnStart;

    [System.Serializable]
    public class Listener
    {
        public IntVariableSO[] intVars;
        public UnityEventFloat onSetUpEvent;
        public UnityEventFloat onSetUpEvent_FloatOutput;
        public UnityEventFloat onStartEvent;
        public UnityEventInt onChangeEvent;
        public UnityEventFloat onChangeEvent_FloatOutput;
        public List<ValueCheckEvent> compareEvents;
    }
    [SerializeField] List<Listener> listeners;

    [System.Serializable]
    public class ValueCheckEvent
    {
        public ENumberCompareMode compareMode;
        public IntReference compareValue;
        [System.Serializable]
        public class Events
        {
            public UnityEventFloat onTrue;
            public UnityEventFloat onFalse;
        }
        public Events responses;
    }

    private void OnEnable()
    {
        foreach (var listener in listeners)
            foreach (var intVar in listener.intVars)
                if (intVar != null)
                    intVar.RegisterListener(this);
    }
    private void OnDisable()
    {
        foreach (var listener in listeners)
            foreach (var intVar in listener.intVars)
                if (intVar != null)
                    intVar.UnregisterListener(this);
    }

    private void Start()
    {
        if (invokeOnStart)
            foreach (var listener in listeners)
                foreach (var intVar in listener.intVars)
                    if (intVar != null)
                    {
                        listener.onStartEvent.Invoke(intVar.Value);
                        listener.onChangeEvent.Invoke(intVar.Value);
                        listener.onChangeEvent_FloatOutput.Invoke(intVar.Value);
                    }
    }

    public void OnSetUpEventRaised(IntVariableSO _intVar)
    {
        if (_intVar == null)
            return;
        Listener listener = listeners.Find(x => x.intVars.ToList().Find(y => y == _intVar));
        if (listener == null)
            return;
        listener.onSetUpEvent.Invoke(_intVar.Value);
        listener.onSetUpEvent_FloatOutput.Invoke((float)_intVar.Value);
    }

    public void OnChangeEventRaised(IntVariableSO _intVar)
    {
        if (_intVar == null)
            return;

        Listener listener = listeners.Find(x => x.intVars.ToList().Find(y => y == _intVar));

        if (listener == null)
            return;

        listener.onChangeEvent.Invoke(_intVar.Value);
        listener.onChangeEvent_FloatOutput.Invoke((float)_intVar.Value);

        if (listener.compareEvents != null && listener.compareEvents.Count > 0)
        {
            foreach (ValueCheckEvent valueCheckEvent in listener.compareEvents)
            {
                switch (valueCheckEvent.compareMode)
                {
                    case ENumberCompareMode.Equals:
                        if (_intVar.Value == valueCheckEvent.compareValue.Value)
                            valueCheckEvent.responses.onTrue.Invoke(_intVar.Value);
                        else
                            valueCheckEvent.responses.onFalse.Invoke(_intVar.Value);
                        break;
                    case ENumberCompareMode.GreaterThan:
                        if (_intVar.Value > valueCheckEvent.compareValue.Value)
                            valueCheckEvent.responses.onTrue.Invoke(_intVar.Value);
                        else
                            valueCheckEvent.responses.onFalse.Invoke(_intVar.Value);
                        break;
                    case ENumberCompareMode.LessThan:
                        if (_intVar.Value < valueCheckEvent.compareValue.Value)
                            valueCheckEvent.responses.onTrue.Invoke(_intVar.Value);
                        else
                            valueCheckEvent.responses.onFalse.Invoke(_intVar.Value);
                        break;
                    case ENumberCompareMode.LessThanOrEqual:
                        if (_intVar.Value <= valueCheckEvent.compareValue.Value)
                            valueCheckEvent.responses.onTrue.Invoke(_intVar.Value);
                        else
                            valueCheckEvent.responses.onFalse.Invoke(_intVar.Value);
                        break;
                    case ENumberCompareMode.GreaterThanOrEqual:
                        if (_intVar.Value >= valueCheckEvent.compareValue.Value)
                            valueCheckEvent.responses.onTrue.Invoke(_intVar.Value);
                        else
                            valueCheckEvent.responses.onFalse.Invoke(_intVar.Value);
                        break;
                }
            }
        }
    }
}