﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FloatVariableListener : MonoBehaviour
{
    [SerializeField] bool invokeOnStart;

    [System.Serializable]
    public class Listener
    {
        public FloatVariableSO[] floatVars;
        public UnityEventFloat onStartEvent;
        public UnityEventFloat onChangeEvent;
        public List<ValueCheckEvent> compareEvents;
    }
    [SerializeField] List<Listener> listeners;

    [System.Serializable]
    public class ValueCheckEvent
    {
        public ENumberCompareMode eCompareMode;
        public FloatReference compareValue;
        [System.Serializable]
        public class Events
        {
            public UnityEventFloat onTrue;
            public UnityEventFloat onFalse;
        }
        public Events responses;
        public void Check(float _value)
        {
            switch (eCompareMode)
            {
                case ENumberCompareMode.Equals:
                    if (_value == compareValue.Value)
                        responses.onTrue.Invoke(_value);
                    else
                        responses.onFalse.Invoke(_value);
                    break;
                case ENumberCompareMode.GreaterThan:
                    if (_value > compareValue.Value)
                        responses.onTrue.Invoke(_value);
                    else
                        responses.onFalse.Invoke(_value);
                    break;
                case ENumberCompareMode.LessThan:
                    if (_value < compareValue.Value)
                        responses.onTrue.Invoke(_value);
                    else
                        responses.onFalse.Invoke(_value);
                    break;
                case ENumberCompareMode.LessThanOrEqual:
                    if (_value <= compareValue.Value)
                        responses.onTrue.Invoke(_value);
                    else
                        responses.onFalse.Invoke(_value);
                    break;
                case ENumberCompareMode.GreaterThanOrEqual:
                    if (_value >= compareValue.Value)
                        responses.onTrue.Invoke(_value);
                    else
                        responses.onFalse.Invoke(_value);
                    break;
            }
        }
    }

    private void OnEnable()
    {
        foreach (var listener in listeners)
            foreach (var floatVar in listener.floatVars)
                if (floatVar != null)
                    floatVar.RegisterListener(this);
    }
    private void OnDisable()
    {
        foreach (var listener in listeners)
            foreach (var floatVar in listener.floatVars)
                if (floatVar != null)
                    floatVar.UnregisterListener(this);
    }

    private void Start()
    {
        if (invokeOnStart)
            foreach (var listener in listeners)
                foreach (var floatVar in listener.floatVars)
                    if (floatVar != null)
                    {
                        listener.onStartEvent.Invoke(floatVar.Value);
                        listener.onChangeEvent.Invoke(floatVar.Value);
                    }
    }

    public void OnChangeEventRaised(FloatVariableSO _floatVar)
    {
        if (_floatVar == null)
            return;

        Listener listener = listeners.Find(_listener => _listener.floatVars.ToList().Find(_listernerVar => _listernerVar == _floatVar));

        if (listener == null)
            return;

        listener.onChangeEvent.Invoke(_floatVar.Value);

        if (listener.compareEvents != null && listener.compareEvents.Count > 0)
        {
            foreach (ValueCheckEvent valueCheckEvent in listener.compareEvents)
                valueCheckEvent.Check(_floatVar.Value);
        }
    }
}