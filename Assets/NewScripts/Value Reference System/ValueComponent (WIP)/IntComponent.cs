﻿using UnityEngine;

public class IntComponent : MonoBehaviour
{
    public string name;
    [SerializeField] IntReference value; public int Value { get { return value.Value; } set { if (this.value.Value == value) return; this.value.Value = value; onChangeEvent.Invoke(value); } }
    [SerializeField] IntReference defaultValue;
    [SerializeField] protected UnityEventInt onChangeEvent;

    private void OnEnable()
    {
        value.Value = defaultValue.Value;
    }
}