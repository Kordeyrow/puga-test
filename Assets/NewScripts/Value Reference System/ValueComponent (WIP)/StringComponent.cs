﻿using UnityEngine;

public class StringComponent : MonoBehaviour
{
    public string name;
    [SerializeField] StringReference value; public string Value { get { return this.value.Value; } set { if (this.value.Value == value) return; this.value.Value = value; onChangeEvent.Invoke(value); } }
    [SerializeField] StringReference defaultValue;
    [SerializeField] protected UnityEventString onChangeEvent;

    private void OnEnable()
    {
        value.Value = defaultValue.Value;
    }
}