﻿using UnityEngine;

public class TransformComponent : MonoBehaviour
{
    public string name;
    [SerializeField] TransformReference value; public Transform Value { get { return this.value.Value; } set { if (this.value.Value == value) return; this.value.Value = value; onChangeEvent.Invoke(value); } }
    [SerializeField] TransformReference defaultValue;
    [SerializeField] protected UnityEventTransform onChangeEvent;

    private void OnEnable()
    {
        value.Value = defaultValue.Value;
    }
}
