﻿using UnityEngine;

[System.Serializable]
public class StringReference : ValueReference
{
    [SerializeField] string constantValue;
    [SerializeField] StringSO SOValue;
    [SerializeField] StringComponent componentValue;

    public string Value
    {
        get
        {
            switch (valueType)
            {
                case 0:
                    return constantValue;
                case 1:
                    return SOValue == null ? "" : SOValue.Value;
                case 2:
                    return componentValue == null ? "" : componentValue.Value;
                default:
                    return "";
            }
        }
        set
        {
            switch (valueType)
            {
                case 0:
                    constantValue = value;
                    break;
                case 1:
                    if (SOValue != null)
                    {
                        StringVariableSO var = (StringVariableSO) SOValue;
                        if(var != null)
                            var.SetValue(value);
                    }
                    break;
                case 2:
                    if (componentValue != null)
                        componentValue.Value = value;
                    break;
            }
        }
    }
}
