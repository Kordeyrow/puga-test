﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISavable {
    int id { get; }
    bool needToSave { get; set; }
    SaveData getSaveData();
    void OnLoad(object _obj);
}

[System.Serializable]
public class SaveData
{
    public int id;
    public string data;

    public SaveData(int _id, object _data)
    {
        id = _id;
        data = JsonUtility.ToJson(_data);
    }
}

[System.Serializable]
public class SaveDataList
{
    public List<SaveData> data;

    public SaveDataList(List<SaveData> _data)
    {
        data = _data;
    }
}