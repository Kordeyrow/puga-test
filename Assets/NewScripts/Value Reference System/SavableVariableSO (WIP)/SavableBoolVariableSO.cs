﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewSavableBoolVariable", menuName = "Values/Savable/Bool (WIP)")]
public class SavableBoolVariableSO : BoolVariableSO
{
    public void Load()
    {

    }

    public void Save()
    {

    }
}