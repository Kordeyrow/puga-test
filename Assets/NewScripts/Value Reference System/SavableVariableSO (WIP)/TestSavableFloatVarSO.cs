﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTestSavableFloatVarSO", menuName = "Values/SaveVariables/Float")]
public class TestSavableFloatVarSO : FloatVariableSO
{
    [SerializeField] IntReference ID;

    protected override void OnEnable()
    {
        //base.OnEnable();
        //Debug.Log("test    " + PlayerPrefs.GetFloat(ID.Value.ToString()));
        _value = PlayerPrefs.GetFloat(ID.Value.ToString());
    }

    public void Load()
    {
        //Debug.Log(1);
   
           _value = PlayerPrefs.GetFloat(ID.Value.ToString());
    }

    private void OnDisable()
    {
        //Debug.Log(2);
        //PlayerPrefs.SetFloat(ID.Value.ToString(), _value);
    }

    public void Save()
    {
        //Debug.Log(3 + "  -  _value " + _value);
        PlayerPrefs.SetFloat(ID.Value.ToString(), _value);
    }

    [ExecuteInEditMode]
    [ContextMenu("Resetar")]
    public void Reset()
    {
        _value = 0;
        PlayerPrefs.SetFloat(ID.Value.ToString(), 0);
    }
}
