﻿using System;
using UnityEngine;

//[CreateAssetMenu(fileName = "NewSavableFloatariable", menuName = "Variables/Savable Float")]
public class SavableFloatVariableSO : FloatVariableSO //, ISavable
{
    //[SerializeField] IntVariableSO ID;
    //bool hasFirstLoaded;
    //bool _needToSave = false;
    //public UnityEventFloat onRaise;

    //public override float Value
    //{
    //    get
    //    {
    //        //if (!hasFirstLoaded)
    //        //    Load();
    //        return _value;
    //    }
    //    set
    //    {
    //        if (_value == value)
    //            return;
    //        _value = value;
    //        needToSave = true;
    //        onRaise.Invoke(_value);
    //        Raise();
    //    }
    //}

    //public bool needToSave
    //{
    //    get
    //    {
    //        return _needToSave;
    //    }

    //    set
    //    {
    //        _needToSave = value;
    //    }
    //}

    //public int id
    //{
    //    get
    //    {
    //        return ID.Value;
    //    }
    //}

    //public void OnLoad(object _obj)
    //{
    //    if (_obj != null)
    //    {
    //        Float obj = JsonUtility.FromJson<Float>((string)_obj);
    //        //TesteSave.addTextL(obj.ToString());

    //        _value = obj.value;
    //        onRaise.Invoke(_value);
    //        //TesteSave.addTextL("FLOATFOI" + ID.Value);
    //    }
    //    else
    //    {
    //        //TesteSave.addTextL("FLOANTFOI" + ID.Value);
    //    }
    //    hasFirstLoaded = true;
    //    Raise();
    //}


    //////Ao alterar qualquer valor via inspector (também chamado quando Unity abre e ao dar play)
    //public void OnValidate()
    //{
    //    if (!hasFirstLoaded)
    //    {
    //        Load();
    //        return;
    //    }
    //    if (ID != null)
    //        SaverLoader.SaveByID(ID.Value, _value);
    //    Raise();
    //}

    //public SaveData getSaveData()
    //{
    //    return new SaveData(ID.Value, new Float(_value));
    //}

    ////private void OnDestroy()
    ////{
    ////    if (ID != null)
    ////        SaverLoader.DeleteFileByID(ID.Value);
    ////}
}

[System.Serializable]
public class Float { public float value; public Float(float _value) { value = _value; } }
