﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewStatusLevel", menuName = "NewStatusLevel")]
public class StatusLevel : ScriptableObject
{
    public IntReference startHealth;
    public IntReference startAttack;
    public FloatReference startSpeed;
}
