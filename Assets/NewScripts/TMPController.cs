﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TMPController : MonoBehaviour
{
    [SerializeField] IntReference decimals;
    [SerializeField] BoolReference isTime;
    TextMeshProUGUI text;

    private void Awake()
    {
        if (text == null)
            text = GetComponent<TextMeshProUGUI>();
    }

    public void SetValue(string _text)
    {
        if (text == null)
            text = GetComponent<TextMeshProUGUI>();

        text.text = _text;
    }
    public void SetValue(StringSO _stringSO)
    {
        if (_stringSO == null)
            return;
        SetValue(_stringSO.Value);
    }
    public void AddValue(string _text)
    {
        SetValue(text.text + _text);
    }
    public void AddValue(StringSO _stringSO)
    {
        if (_stringSO == null)
            return;
        AddValue(text.text);
    }

    public void SetValue(float _value)
    {
        string s = _value.ToString();

        if (isTime.Value)
        {
            float minutes = Mathf.FloorToInt((_value+1) / 60);
            float seconds = Mathf.FloorToInt((_value+1) % 60);
            if(_value == 0)
                s = string.Format("00:00");
            else
                s = string.Format("{0:00}:{1:00}", minutes, seconds);
        }
        else if (decimals.Value > 0)
        {
            string decimalFormat = "F" + decimals.Value.ToString("F0");
            s = _value.ToString(decimalFormat);
        }
        SetValue(s);
    }
    public void SetValue(FloatSO _floatSO)
    {
        if (_floatSO == null)
            return;
        SetValue(_floatSO.Value);
    }
    public void AddValue(float _value)
    {
        if (decimals.Value > 0)
        {
            string decimalFormat = "F" + decimals.Value.ToString("F0");
            AddValue(_value.ToString(decimalFormat));
        }
        else
            AddValue(_value.ToString());
    }
    public void AddValue(FloatSO _floatSO)
    {
        if (_floatSO == null)
            return;
        AddValue(_floatSO.Value);
    }

    public void SetValue(int _value)
    {
        SetValue(_value.ToString());
    }
    public void SetValue(IntSO _intSO)
    {
        if (_intSO == null)
            return;
        SetValue(_intSO.Value);
    }
    public void AddValue(int _intSO)
    {
        AddValue(_intSO.ToString());
    }
    public void AddValue(IntSO _intSO)
    {
        AddValue(_intSO.Value);
    }
}
