﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnAppPauseEvent : MonoBehaviour
{
    [SerializeField] UnityEvent onAppPause;
    [SerializeField] UnityEvent onAppUnpause;

    private void OnApplicationPause(bool pause)
    {
        if (pause)
            onAppPause.Invoke();
        else
            onAppUnpause.Invoke();
    }
}
