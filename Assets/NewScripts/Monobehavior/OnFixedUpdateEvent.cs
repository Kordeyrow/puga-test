﻿using UnityEngine;
using UnityEngine.Events;

public class OnFixedUpdateEvent : MonoBehaviour
{
    [SerializeField] UnityEvent onFixedUpdate;

    private void FixedUpdate()
    {
        onFixedUpdate.Invoke();
    }
}